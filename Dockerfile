FROM codewithweeb/weebzone:stable

WORKDIR /usr/src/app
RUN chmod 777 /usr/src/app

RUN apt -qq update --fix-missing && \
    apt -qq install -y \
    mediainfo

COPY . .
RUN curl -o /usr/src/app/requirements.txt https://gitlab.com/yopremium21/wzml-deploy/-/snippets/2524881/raw/main/requirements.txt

RUN pip3 install --no-cache-dir -r requirements.txt
RUN rm -r /usr/src/app/requirements.txt


CMD ["bash", "start.sh"]
